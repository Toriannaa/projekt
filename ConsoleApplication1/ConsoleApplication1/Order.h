#pragma once
#pragma
#include <iostream>
#include "pch.h"
#include "Coffee.h"
#include "Ciasto.h"
#include "Kanapka.h"
using namespace std;

class Order
{
	mutable Coffee* wsk;
	mutable Ciasto* ciastko;
	mutable Kanapka* kanapeczka;
	float cena;
	int ilosc;
	int ilosck;
	int iloscc;
	int iloscK;


public:

	void discount(float _money, float discount);

	Order(const Order* &order)
	{
		// "tylko" formalny
		cena = order->cena;
		ilosc = order->ilosc;
		ilosck = order->ilosck;
		iloscc = order->iloscc;
		iloscK = order->iloscK;
		wsk = order->wsk;
		ciastko = order->ciastko;
		kanapeczka = order->kanapeczka;
	}
	Order(float x = 0, int y = 0, int z = 0, int zz = 0, int zzz = 0) {
		cena = x;
		ilosc = y;
		ilosck = z;
		iloscK = z;
		iloscc = zz;
	}
	~Order()
	{

	}

	const Coffee* Pobieranie_kawy();
	const Coffee* Pobieranie2_kawy();


	void set_ilosc(int i);

	int get_ilosc();

	void set_ilosck(int i);

	int get_ilosck();

	void set_iloscc(int i);

	int get_iloscc();

	void set_iloscK(int i);

	int get_iloscK();

	void set_cena(float c);

	float get_cena();



	//Alokowanie pami�ci na tablice obiekt�w dla klasy Coffee
	void allocMemory(Coffee *&data, int size);



	//Wpisywanie danych dla kawy

	void prepareData(Coffee *&c, int size2, int index);
	//Wy�wietlanie dla kawy

	void printClear(Coffee*& data, int size);



	//usuwane zaalokowanej pami�ci dla kawy


	void printClear(Coffee*& data);



	const Ciasto* Pobieranie_ciasta();
	const Ciasto* Pobieranie2_ciasta();

	//Alokowanie pami�ci na tablice obiekt�w dla klasy Ciastko
	void allocMemory(Ciasto *&data, int size);


	//Wpisywanie danych dla ciastka

	void prepareData(Ciasto *&c, int size3, int index);

	//Wy�wietlanie dla ciasta

	void printClear(Ciasto*& data, int size);




	const Kanapka* Pobieranie_kanapki();


	const Kanapka* Pobieranie2_kanapeczki();

	//Alokowanie pami�ci na tablice obiekt�w dla klasy kanapka
	void allocMemory(Kanapka *&data, int size);



	//Wpisywanie danych dla ciastka

	void prepareData(Kanapka *&k, int size4, int index);

	//Wy�wietlanie dla kanapki

	void printClear(Kanapka*& data, int size);



	//usuwane zaalokowanej pami�ci dla kanapka


	void printClear(Kanapka*& data);



};

