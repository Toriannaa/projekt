#pragma once
#pragma
#include <iostream>
#include "pch.h"
using namespace std;


class Ciasto
{

	float cenaszt;
	int kind;
	int ilosc;

public:

	Ciasto(float c = 0, int k = 0, int i = 0)
	{
		cenaszt = c;
		kind = k;
		ilosc = i;
	}
	~Ciasto()
	{

	}


	void set_cenaszt(float cs);

	float get_cenaszt();

	void set_kind(int k);

	int get_kind();
	void set_ilosc(int i);

	int get_ilosc();

};