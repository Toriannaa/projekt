#include <iostream>
#include "pch.h"
#include "Coffee.h"
using namespace std;


void Coffee::set_cenak(float c) { cena = c; }

float Coffee::get_cenak() { return cena; }

void Coffee::set_cenaszt(float cs) { cenaszt = cs; }

float Coffee::get_cenaszt() { return cenaszt; }

void Coffee::set_kind(int k) { kind = k; }

int Coffee::get_kind() { return kind; }

void Coffee::set_ilosc(int i) { ilosc = i; }

int Coffee::get_ilosc() { return ilosc; }