#pragma once
#pragma
#include <iostream>
#include "pch.h"
using namespace std;

class Kanapka
{

	float cenaszt;
	int kind;
	int ilosc;

public:
	Kanapka(float c = 0, int k = 0, int i = 0)
	{
		cenaszt = c;
		kind = k;
		ilosc = i;
	}
	~Kanapka()
	{

	};


	void set_cenaszt(float cs);

	float get_cenaszt();

	void set_kind(int k);

	int get_kind();

	void set_ilosc(int i);

	int get_ilosc();

};
